<?

  /**
   * Welche Klassen werden benötigt?
   * 
   * - DB
   * - Anwendung
   * - MVC (Model, View, Controller)
   */

  /**
   * DB
   * 
   * - Verbindungsaufbau
   * - Ausführung
   * - Fetching für einen oder mehrere Datensätze
   * - Shorthand CRUD/RUDI Methoden
   */

   $db->connect();
   
   $db->execute();
   
   $db->select();
   $db->insert();
   $db->update();
   $db->delete();
   
   $db->query();
   $db->queryOne();
   
  /**
   * Anwendung
   * 
   * - Konstuctor
   * - Dispatcher
   * - Fehlermeldungen
   */
   
   $app->get();
   $app->run();

  /**
   * Model
   * 
   * - Einfache Datenbankabfragen
   * - Tabelle
   * - Primärschlüssel
   * - automatisches Mapping auf aktives Model
   * - einfache Berfüllung durch Massive-Assignment
   * - einfache Validierung
   * - Shorthand CRUD/RUDI Methoden
   */
   
   Model::findAll();
   Model::findOne();
   
   Model::setAttributes();
   Model::validate();
   
   Model::insert();
   Model::update();
   Model::delete();

  /**
   * View
   * 
   * - Dateien einbinden und Argumente übergeben
   * - Fehlermeldungen
   */
   
   View::render();

  /**
   * Controller
   * 
   * - Actions
   * - Fehlermeldungen
   * - Shorthand Render-Methode
   */
   
   $controller->render();