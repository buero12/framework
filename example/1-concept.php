<?

  /**
   * Konzept
   */

  /**
   * URL muss identifiziert werden
   * 
   * - localhost/meine-webseite/
   * - localhost/meine-webseite/startseite.html
   */

  print '<pre>';
  print_r($_SERVER);
  print '</pre>';
  
  /**
   * Jede URL muss auf die Anwendungsdatei zeigen
   * 
   * RewriteEngine on
   * RewriteCond %{REQUEST_FILENAME} !-f
   * RewriteCond %{REQUEST_FILENAME} !-d
   * RewriteRule . index.php
   */
  
  /**
   * Für bestimmte URLs müssen Aktionen hinterlegt werden
   * 
   * - / => Startseite
   * - /(\w+).html => beliebige Seite
   */
  
  $routes = [];
  $routes['/(\w+).html'] = function($page) {
    
    // do smth.
    
  };
  
  /**
   * URLs müssen ausgewertet
   */
  
  if (preg_match($pattern, $url, $matches)) {
    
    // do smth.
    
  }

  /**
   * Dateien müssen abhängig von der URL geladen werden
   */
  
  $filePath = 'home.php';
  
  if (file_exists($filePath)) {
    include $filePath;
  } else {
    // Fehlermeldung
  }