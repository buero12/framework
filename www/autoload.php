<?
  
  spl_autoload_register(function($class) {

    $prefixes = [
      'mvc\\' => __DIR__.'/src/',
      'app\\' => __DIR__.'/app/'
    ];
    
    foreach ($prefixes as $prefix => $path) {
      
      $len = strlen($prefix);

      if (strncmp($prefix, $class, $len) !== 0) {
        continue;
      }

      $file = $path.str_replace('\\', '/', substr($class, $len)).'.php';
      
      if (file_exists($file)) {
        require_once($file);
      }
            
    }
    
  });