<?
  
  require_once('../autoload.php');
    
  \mvc\Db::connect('guestbook');
  
  $app = new \mvc\App;
  
  $app->get('/', 'app\controllers\Site::index')
    ->get('/add.html', 'app\controllers\Site::cds')
    ->get('/(\w+).html', 'app\controllers\Site::page');
  
  $app->get('/admin', 'app\controllers\Admin::index');
  
?>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Gästebuch</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>
    
    <div class="container">
      
      <nav class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Gästebuch</a>
        </div>

        <div class="collapse navbar-collapse" id="nav">
          <ul class="nav navbar-nav">
            
            
            
          </ul>
        </div>
      </nav>
      
      <?= $app->run(); ?>
      
    </div>

  </body>
  
</html>