<?

  namespace mvc;

  class Db
  {
    
    protected static $db;
    
    protected $pdo;
    protected $stmt;
    
    public static function connect($db, $user = 'root', $password = null, $host = 'localhost', $port = 3306)
    {
      
      if (is_null(self::$db)) {
        self::$db = new self($db, $user, $password, $host, $port);
      }
      
      return self::$db;
      
    }
    
    public static function disconnect()
    {
      unset(self::$db);
    }
    
    public static function getInstance()
    {
      return isset(self::$db) ? self::$db : null; 
    }
    
    protected function __construct($db, $user, $password, $host, $port)
    {
      $this->pdo = new \PDO("mysql:{$host};port={$port};dbname={$db}", $user, $password); 
    }
    
    public function select($select, $table, array $condition = [], array $params = [], array $order = [], $limit = null, $offset = 0)
    {
      
      $where = $this->buildConditions($condition, $params);
      $order = $this->buildOrder($order);
      $limit = $this->buildLimit($limit);
      
      return $this->execute("SELECT {$select} FROM `{$table}` {$where} {$order} {$limit}", $params);
      
    }
    
    public function buildOrder($order)
    {
      
      foreach ($order as $column => &$sort) {
        $sort = "`{$column}` {$sort}";
      }
      
      $order = implode(', ', $order);
      
      return empty($order) ? '' : "ORDER BY {$order}";
      
    }
    
    public function buildLimit($limit, $offset = 0)
    {
      
      if (is_int($limit)) {
        return "LIMIT {$offset}, {$limit}";
      }
      
      return '';
      
    }
    
    public function insert($table, array $attributes)
    {
      
      $params = [];
      $fields = [];
      $values = [];
      
      foreach ($attributes as $field => $value) {
        
        $i = count($params);
        $p = ":p{$i}";

        $params[$p] = $value;
        $fields[] = "`{$field}`";
        $values[] = $p;
        
      }
      
      $fields = implode(', ', $fields);
            
      return $this->execute("INSERT INTO `{$table}` ({$fields}) VALUES ({$values})", $params);
      
    }
    
    public function update($table, array $attributes, array $condition = [], array $params = [])
    {
           
      $fields = [];
      
      foreach ($attributes as $field => $value) {
        
        $i = count($params);
        $p = ":p{$i}";
        
        $fields[] = "`{$field}` = {$p}";
        $params[$p] = $value;
                
      }
            
      $fields = implode(', ', $fields);
      $where = $this->buildConditions($condition, $params);
      
      return $this->execute("UPDATE `{$table}` SET {$fields} {$where}", $params);
      
    }
    
    public function delete($table, array $condition = [], array $params = [])
    {
            
      $where = $this->buildConditions($condition, $params);
      
      return $this->execute("DELETE FROM `{$table}` {$where}", $params);
      
    }
     
    public function execute($query, array $params = [])
    {
      
      $this->stmt = $this->pdo->prepare($query);
			$this->stmt->execute($params);
      
			return $this;
      
    }
    
    protected function buildConditions($where, array &$params = [])
    {
                          
      foreach ($where as $key => &$value) {

        if (is_int($key) === false) {

          if (is_null($value)) { 
            
            $value = $this->buildNullCondition($key, $value);
            
          } elseif (is_array($value)) {
            
            $value = $this->buildInCondition($key, $value, $params);
            
          } else {
            
            $value = $this->buildSimpleCondition($key, $value, $params);
            
          }

        }

      }
      
      return empty($where) ? '' : 'WHERE '.implode(' AND ', $where);
      
    }
    
    protected function buildSimpleCondition($key, $value, &$params)
    {
      
      $i = count($params);
      $p = ":p{$i}";

      $params[$p] = $value;

      return "`{$key}` = {$p}";  
      
    }
    
    protected function buildNullCondition($key, $value)
    {
      return "`{$key}` IS NULL";
    }
    
    protected function buildInCondition($key, $values, &$params)
    {
      
      $in = [];

      foreach ($values as $value) {

        $i = count($params);
        $p = ":p{$i}";

        $in[] = $p;
        $params[$p] = $value;

      }

      return "`{$key}` IN ({".implode(', ', $in)."})";
      
    }
    
    protected function setFetchMode($className)
    {
      
      if (!is_null($className) && class_exists($className)) {
        $this->stmt->setFetchMode(\PDO::FETCH_INTO, new $className(false));
      }
      
    }

    public function query($className = 'stdClass')
    {
      
      $this->setFetchMode($className);
      
      if (is_null($className)) {
        $results = $this->stmt->fetchAll();
      } else {
        
        $results = [];

        while ($object = $this->stmt->fetch()) {
          $results[] = clone $object;
        }
        
      }
      
      $this->stmt->closeCursor();
      unset($this->stmt);

      return $results;
      
    }
    
    public function queryOne($className = 'stdClass')
    {
      
      $this->setFetchMode($className);

      $result = $this->stmt->fetch();
      
      if (!isset($result) || $result === false) {
        $result = null;
      }
      
      $this->stmt->closeCursor();
      unset($this->stmt);
      
      return $result;
      
    }
    
  }