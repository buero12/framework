<?
  
  namespace mvc;

  class App
  {
      
    protected $routes = [];
    
    public function get($pattern, $callback)
    {
      
      $pattern = addcslashes($pattern, '/');
      
      if (!array_key_exists($pattern, $this->routes)) {
        $this->routes[$pattern] = $callback;
      }

      return $this;
      
    }
    
    protected function getRequestUri()
    {
      
      $baseUri = strrev(explode('/', strrev($_SERVER['SCRIPT_NAME']), 2)[1]);
      return substr(explode('?', $_SERVER['REQUEST_URI'])[0], strlen($baseUri));
      
    }
    
    public function run()
    {
            
      $requestUri = $this->getRequestUri();
      
      foreach ($this->routes as $pattern => $callback) {
        
        $matches = $this->matchRoute($requestUri, $pattern);
        
        if ($matches !== false) {
          
          echo $this->callControllerAction($callback, $matches);
          exit();
          
        }
        
      }
      
      exit('the requested path does not match any route');
            
    }
    
    protected function callControllerAction($callback, $matches)
    {
      
      list($controller, $action) = explode('::', $callback);
      
      $controller = "{$controller}Controller";
      $action = 'action'.ucfirst($action);

      $controller = new $controller;

      if (is_callable(array($controller, $action))) {

        return call_user_func_array(array($controller, $action), $matches);

      } else {
        exit('action not found');
      }

      exit('controller not found');
      
    }
      
    protected function matchRoute($requestUri, $pattern)
    {
      
      if (preg_match('/^'.$pattern.'$/is', $requestUri, $matches)) {
        
        array_shift($matches);
        
        return $matches;
        
      }
      
      return false;
      
    }
    
  }