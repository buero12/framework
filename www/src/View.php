<?
  
  namespace mvc;

  abstract class View
  {
    
    public static function render($template, array $arguments = [])
    {
      
      $templatePath = __DIR__.'/../app/views/'.$template.'.php';
      
      if (!file_exists($templatePath)) {
        exit('view does not exist');
      }
      
      $renderFunction = function() {
        
        extract(func_get_arg(1));
        
        ob_start();
        include func_get_arg(0);
        $content = ob_get_contents();
        ob_end_clean();
        
        return $content;
        
      };
      
      return $renderFunction($templatePath, $arguments);
            
    }
    
  }
