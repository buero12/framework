<?

  namespace mvc;

  class Controller
  {
    
    public function render($template, array $arguments = [])
    {

      $template = $this->getId().'/'.$template;
      
      return View::render($template, $arguments);
      
    }
    
    public function getId()
    {
      
      $reflect = new \ReflectionClass($this);
      
      $shortName = $reflect->getShortName();
      
      $id = preg_replace('/^(\w+)Controller$/', '$1', $shortName);
      
      return lcfirst($id);
      
    }
    
  }