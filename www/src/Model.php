<?

  namespace mvc;

  abstract class Model
  {
    
    public static abstract function table();
    public static abstract function primaryKey();
    public static abstract function fields();
    public static abstract function rules();
    public static abstract function labels();
    
    public function getFormName($field)
    {
      return $this->getId().'['.$field.']';
    }
    
    public function getId()
    {
      
      $reflect = new \ReflectionClass($this);
      
      return $reflect->getShortName();
      
    }
    
    public static function getLabel($field)
    {
      
      $labels = $this->labels();
      
      return array_key_exists($field, $labels) ? $labels[$field] : null;
      
    }
    
    protected $errors = [];
    
    public static function findAll($attr = [], array $params = [], array $order = [], $limit = null, $offset = 0)
    {
      return self::findInternal($attr, $params, $order, $limit, $offset)->query(get_called_class());
    }
    
    public static function findOne($attr = [], array $params = [], array $order = [])
    {
      return self::findInternal($attr, $params, $order)->queryOne(get_called_class());
    }
    
    protected static function findInternal($attr, array $params, array $order, $limit = null, $offset = 0)
    {
      
      $className = get_called_class();
                  
      if (!is_array($attr)) {
        $attr = [$className::primaryKey() => $attr];
      }
      
      return Db::getInstance()->select('*', $className::table(), $attr, $params, $order, $limit, $offset);
      
    }
    
    public function setAttributes(array $attr)
    {
      
      $className = get_called_class();
      
      $fields = $className::fields();
      
      $attr = array_intersect_key($attr, array_flip($fields));
      
      foreach ($attr as $key => $value) {
        $this->$key = $value;
      }
      
    }
    
    public function validate()
    {
      
      $className = get_called_class();
      
      $rules = $className::rules();
      $fields = $className::fields();
      
      $rules = array_intersect_key($rules, array_flip($fields));
      
      foreach ($rules as $field => $validators) {
        
        $errors = [];
        
        foreach ($validators as $validator => $args) {

          $validator = self::getValidator($validator);

          $valid = $validator::validate($this->$field, $args);
          
          if ($valid !== true) {
            $errors[] = $valid;
          }
          
        }
        
        if (!empty($errors)) {
          $this->errors[$field] = $errors;
        }
        
      }
      
      return $this->hasErrors();
      
    }
    
    protected static function getValidator($validator)
    {
      return '\\mvc\\validators\\'.ucfirst($validator).'Validator';
    }
    
    public function hasErrors($field = null)
    {
      
      if (is_null($field)) {
        return !empty($this->errors);
      }
      
      return array_key_exists($field, $this->errors) ? !empty($this->errors[$field]) : false;
      
    }
    
    public function getErrors($field = null)
    {
      
      if (is_null($field)) {
        return $this->errors;
      }
      
      return array_key_exists($field, $this->errors) ? $this->errors[$field] : [];
      
    }
    
    public function insert()
    {
      
      $className = get_called_class();
      
      $fields = $this->populateFields();
            
      return Db::getInstance()->insert($className::table(), $fields);
      
    }
    
    public function update()
    {
       
      $className = get_called_class();
      $primaryKey = $className::primaryKey();
      
      $fields = $this->populateFields();
      $condition = [$primaryKey => $this->$primaryKey];
                  
      return Db::getInstance()->update($className::table(), $fields, $condition);
      
    }
    
    protected function populateFields()
    {
      
      $className = get_called_class();
      
      $fields = [];
      
      foreach ($className::fields() as $field) {
        
        if (isset($this->$field)) {
          $fields[$field] = $this->$field;
        }
        
      }
      
      return $fields;
      
    }
    
    public function delete()
    {
      
      $className = get_called_class();
      $primaryKey = $className::primaryKey();
      
      $condition = [$primaryKey => $this->$primaryKey];
      
      return Db::getInstance()->delete($className::table(), $condition);
      
    }
    
  }