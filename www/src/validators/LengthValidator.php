<?

  namespace mvc\validators;
  
  abstract class LengthValidator
  {
    
    const ERROR_LENGTH = 'length';
    const ERROR_MIN_MAX_LENGTH = 'minMaxLength';
    const ERROR_MIN_LENGTH = 'minLength';
    const ERROR_MAX_LENGTH = 'maxLength';
    
    public static function validate($value, $args = [])
    {
      
      $length = strlen($value);
      
      if (isset($args['n'])) {
        return $length === $args['n'] ? true : self::ERROR_LENGTH;
      }
      
      if (isset($args['min'], $args['max'])) {
        return $length > $args['min'] && $length < $args['max'] ? true : self::ERROR_MIN_MAX_LENGTH;
      }
      
      if (isset($args['min'])) {
        return $length > $args['min'] ? true : self::ERROR_MIN_LENGTH;
      }
      
      if (isset($args['max'])) {
        return $length < $args['max'] ? true : self::ERROR_MAX_LENGTH;
      }
      
      return true;
      
    }
    
  }