<?

  namespace mvc\validators;

  abstract class RequiredValidator
  {
    
    const ERROR_REQUIRED = 'required';
    
    public static function validate($value, $args = null)
    {
      return isset($value) ? true : self::ERROR_REQUIRED;
    }
    
  }