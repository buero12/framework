<?

  namespace mvc\validators;
  
  abstract class NotBlankValidator
  {
    
    const ERROR_NOT_BLANK = 'notBlank';
    
    public static function validate($value, $args = null)
    {
      return empty($value) ? self::ERROR_NOT_BLANK : true;
    }
    
  }