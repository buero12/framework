<?

  namespace mvc\validators;
  
  abstract class EmailValidator
  {
    
    const ERROR_EMAIL = 'email';
    
    public static function validate($value, $args = null)
    {
      return filter_var($value, FILTER_VALIDATE_EMAIL) ? true : self::ERROR_EMAIL;     
    }
    
  }