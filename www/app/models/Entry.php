<?

  namespace app\models;

  class Entry extends \mvc\Model
  {
    
    public $id;
    public $pid;
    public $created;
    public $user_id;
    public $subject;
    public $comment;
    public $visible;
    
    public static function table()
    {
      return 'entry';
    }
    
    public static function primaryKey()
    {
      return 'id';
    }
    
    public static function fields()
    {
      return ['id', 'pid', 'created', 'user_id', 'subject', 'comment', 'visible'];
    }
    
    public static function labels()
    {
      return [

      ];
    }
    
    public static function rules()
    {
      
      return [
        
      ];
      
    }
    
    public function getUser()
    {
      return User::findOne($this->user_id);
    }
    
    public function getDate($format)
    {
      return (new \DateTime($this->created))->format($format);
    }
    
  }