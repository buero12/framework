<?

  namespace app\models;

  class User extends \mvc\Model
  {
    
    public $id;
    public $email;
    public $password;
    public $name;
    public $admin;
    public $status;
    
    public static function table()
    {
      return 'user';
    }
    
    public static function primaryKey()
    {
      return 'id';
    }
    
    public static function fields()
    {
      return ['id', 'email', 'password', 'name', 'admin', 'status'];
    }
    
    public static function labels()
    {
      return [

      ];
    }
    
    public static function rules()
    {
      
      return [
        
      ];
      
    }
    
    public function getGravatar($size = 100)
    {
      return '//gravatar.com/avatar/'.md5($this->email).'?s='.$size;
    }
    
  }