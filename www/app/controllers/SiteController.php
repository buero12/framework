<?

  namespace app\controllers;
  
  use app\models\Entry;
  use app\models\User;
  
  class SiteController extends \mvc\Controller
  {
    
    public function actionIndex()
    {
      
      $entries = Entry::findAll([
        'visible' => 1
      ], [], [
        'created' => 'DESC'
      ]);
      
      return $this->render('index', [
        'entries' => $entries
      ]);
      
    }
    
    public function actionAdd()
    {
      
      $entry = new Entry;
      
    }
    
    public function actionPage($page)
    {
      
      return $this->render($page);
      
    }
    
  }