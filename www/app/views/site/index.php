<? foreach ($entries as $entry) : ?>

<div class="media">
  <div class="media-left">
    <img class="media-object" src="<?= $entry->getUser()->getGravatar(); ?>" class="img-circle">
  </div>
  <div class="media-body">
    <h4 class="media-heading">
      <?= $entry->subject; ?>
      <small><?= $entry->getUser()->name; ?> schrieb am <?= $entry->getDate('d.m.Y'); ?> um <?= $entry->getDate('H:i:s'); ?> Uhr</small>
    </h4>
    <?= $entry->comment; ?>
    
    <div class="clearfix">
      
      <a href="/add.html?id=<?= $entry->id; ?>" class="btn btn-sm btn-primary">antworten</a>
      
    </div>
    
  </div>
</div>

<hr />

<? endforeach;